require("dotenv").load();

const express = require("express");
const cors = require("cors");
const nodemailer = require("nodemailer");
var sgTransport = require("nodemailer-sendgrid-transport");

const app = express();
const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post("/notify/now", (req, res) => {
  nodemailer.createTestAccount((err, account) => {
    if (err) {
      console.error("Failed to create a testing account. " + err.message);
      res.status(500).send("Error occurred: " + err.message);
    } else {
      console.log("Credentials obtained, sending message...");

      var options = {
        auth: {
          api_user: process.env.SENDGRID_USERNAME,
          api_key: process.env.SENDGRID_PASSWORD
        }
      };

      // Create a SMTP transporter object
      let transporter = nodemailer.createTransport(sgTransport(options));

      console.log(req.body);

      let recipient = req.body.recipient;
      let subject = req.body.subject;
      let content = req.body.content;
      let content_html = req.body.content_html;

      // Message object
      let message = {
        from: "Mailme Notifier <mailme@idopterlabs.com.br>",
        to: recipient,
        subject: subject,
        text: content,
        html: content_html
      };

      transporter.sendMail(message, (err, info) => {
        if (err) {
          console.log("Error occurred: %s", err.message);
          res.status(500).send("Error occurred: " + err.message);
        } else {
          console.log("Message sent: %s", info.messageId);
          res.status(200).send("Message sent!");
        }
      });
    }
  });
});

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  console.log("Express server listening on port " + port);
});
